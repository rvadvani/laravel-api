<?php
namespace App\Policies;

use App\User;
use App\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function update(User $user, Article $article)
    {
        return $user->ownsArticle($article);
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function destroy(User $user, Article $article)
    {
        return $user->ownsArticle($article);
    }

}