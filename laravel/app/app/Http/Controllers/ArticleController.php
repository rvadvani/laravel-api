<?php

namespace App\Http\Controllers;

use App\Article;
use App\Transformer\ArticleTransformer;
use App\Transformer\UserTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(5);
        $articlesCollection = $articles->getCollection();

        return fractal()
            ->collection($articlesCollection)
            ->parseIncludes(['users'])
            ->transformWith(new ArticleTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($articles));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'tag_id' => 'required|numeric',
        ]);

        //Article::create($data);
        $article = new Article;
        $article->title = $request->title;
        $article->body = $request->body;
        $article->tag_id = $request->tag_id;
        $article->user()->associate($request->user());
        $article->save();

        return fractal()
            ->item($article)
            ->parseIncludes('user')
            ->transformWith(new ArticleTransformer)
            ->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //s = ::find($id);

        return fractal()
            ->item($article)
            ->parseIncludes(['user'])
            ->transformWith(new ArticleTransformer)
            ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $this->authorize('update', $article);
        $article->title = $request->get('title', $article->title);
        $article->body = $request->get('body', $article->body);
        $article->save();

        return fractal()
            ->item($article)
            ->parseIncludes(['user'])
            ->transformWith(new ArticleTransformer)
            ->toArray();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $this->authorize('destroy', $article);
        $article->delete();
        return response(null, 204);
    }
}
