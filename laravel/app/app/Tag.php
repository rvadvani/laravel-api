<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tagname'];
    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
