<?php
/**
 * Created by PhpStorm.
 * User: Coder Seven
 * Date: 01-Nov-17
 * Time: 12:32 PM
 */

namespace App\Transformer;


use App\Topics;
class TopicTransformer extends \League\Fractal\TransformerAbstract
{
    protected $availableIncludes = ['user', 'posts'];

    /**
     * @param Topics $topic
     * @return array
     */
    public function transform(Topics $topic)
    {
        return [
            'id' => $topic->id,
            'title' => $topic->title,
            'created_at' => $topic->created_at->toDateTimeString(),
            'created_at_human' => $topic->created_at->diffForHumans()
        ];
    }

    public function includeUser(Topics $topic)
    {
        return $this->item($topic->user, new UserTransformer);
    }

    public function includePosts(Topics $topic)
    {
        return $this->collection($topic->posts, new PostTransformer);
    }
}