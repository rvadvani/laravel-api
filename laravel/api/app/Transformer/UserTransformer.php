<?php
/**
 * Created by PhpStorm.
 * User: Coder Seven
 * Date: 31-Oct-17
 * Time: 1:03 AM
 */
namespace App\Transformer;

use App\User;

class UserTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        return [
          'username' => $user->name,
          'avatar' => $user->avatar(),
        ];
    }
}