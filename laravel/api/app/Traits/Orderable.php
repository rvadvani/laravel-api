<?php
/**
 * Created by PhpStorm.
 * User: Coder Seven
 * Date: 01-Nov-17
 * Time: 12:10 PM
 */
namespace App\Traits;

trait Orderable
{
    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
    public function scopeOldestFirst($query)
    {
        return $query->orderBy('created_at', 'asc');
    }
}
