<?php
/**
 * Created by PhpStorm.
 * User: Coder Seven
 * Date: 01-Nov-17
 * Time: 1:54 PM
 */
namespace App\Policies;

use App\Topics;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TopicPolicy
{
    use HandlesAuthorization;
    public function update(User $user, Topics $topic)
    {
        return $user->ownsTopic($topic);
    }

    public function destroy(User $user, Topics $topic)
    {
        return $user->ownsTopic($topic);
    }
}