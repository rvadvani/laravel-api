<?php

namespace App\Http\Controllers;

use App\Post;
use App\Topics;
use App\Transformer\PostTransformer;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(Request $request, Topics $topic)
    {
        $post = new Post;
        $post->body = $request->body;
        $post->user()->associate($request->user());
        $topic->posts()->save($post);
        return fractal()
            ->item($post)
            ->parseIncludes(['user'])
            ->transformWith(new PostTransformer)
            ->toArray();
    }

    public function update(Request $request, Topics $topic, Post $post)
    {
        $this->validate($request, ['body'=>'required|max:2000']);
        $this->authorize('update', $post);
        $post->body = $request->get('body', $post->body);
        $post->save();

        return fractal()
            ->item($post)
            ->parseIncludes(['user'])
            ->transformWith(new PostTransformer)
            ->toArray();
    }

    public function destroy(Topics $topic, Post $post)
    {
        $this->authorize('destroy', $post);
        $post->delete();
        return response(null, 204);
    }
}
