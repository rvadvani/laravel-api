<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\Sending;
class MailController extends Controller
{
    public function mail()
    {
        return view('mail');
    }

    public function send()
    {
        Mail::send(new Sending());
        echo "sent successfully";
    }
    public function view()
    {
        return view('content', ['msg' => 'Coder Seven']);
    }
}
