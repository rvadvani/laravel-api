<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send Mail</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<br>
<div class="row">
    <div class="col-md-4 offset-4">
        <form action="{{ route('send') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label>To</label>
                <input type="text" class="form-control" name="to" placeholder="username@domain.com">
            </div>
            <div class="form-group">
                <label>Message</label>
                <textarea type="text" class="form-control" name="message" placeholder="compose message"></textarea>
            </div>
            <button class="btn btn-primary">Send</button>
        </form>
    </div>
</div>

</body>
</html>