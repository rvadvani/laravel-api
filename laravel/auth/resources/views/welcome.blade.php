<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Auth</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>


<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
    <div class="container">
        <a href="{{ url('/') }}" class="navbar-brand">{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/blog') }}">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/help') }}">Help</a>
                </li>
            </ul>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.login') }}" target="_blank">Docs</a>
                </li>
                @guest()
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download">User <span class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="download">
                            <a class="dropdown-item" href="{{ route('login') }}">Login</a>
                            <a class="dropdown-item" href="{{ route('register') }}">Regiter</a>
                        </div>
                    </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">User Logged In</a>
                        </li>
                    @endguest

                @guest('admin')
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download">Admin <span class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="download">
                            <a class="dropdown-item" href="{{ route('admin.login') }}">Login</a>
                            <a class="dropdown-item" href="{{ route('admin.register') }}">Regiter</a>
                        </div>
                    </li>
                @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.home') }}">Admin Logged In</a>
                        </li>
                @endguest
            </ul>

        </div>
    </div>
</div>
<br><br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-3" align="center">

            <div class="list-group">
                <a href="{{ url('/') }}" class="list-group-item list-group-item-action active">Home</a>
                <a href="{{ route('admin.login') }}" class="list-group-item list-group-item-action">Admin</a>
                <a href="{{ route('login') }}" class="list-group-item list-group-item-action">User</a>
                <a href="{{ route('mail') }}" class="list-group-item list-group-item-action">Mails</a>
                <a href="{{ url('/') }}" class="list-group-item list-group-item-action disabled">more</a>
            </div>

        </div>
    </div>
</div>



<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>