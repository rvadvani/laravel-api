<?php

namespace App\Http\Controllers;

use Auth;
use App\Article;
use App\Topic;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::get();
        $articles = Article::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->paginate(5);
        return view('home', compact('articles', 'topics'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $data = $this->validate($request, [
            'topic_id' => 'required',
            'title' => 'required',
            'body' => 'required'
        ]);

        //Article::create($data);
        $article = new Article($data);
        Auth::user()->articles()->save($article);

        return redirect()->to(route('home'));
    }

    public function view($id)
    {
        $article = Article::find($id);
        return view('view', compact('article'));
    }
}
