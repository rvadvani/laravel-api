<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
</head>
<body>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form action="{{ route('form.submit') }}" method="post">
                {{ csrf_field() }}
                <input type="text" name="title">
                <input type="text" name="body">
                <button>Submit</button>
            </form>
        </div>
        <div class="col-sm-6">
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Body</th>
                </tr>

                @foreach($articles as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->body }}</td>
                    </tr>
                @endforeach

            </table>

            {{ $articles->links() }}

        </div>

    </div>
</div>



</body>
</html>