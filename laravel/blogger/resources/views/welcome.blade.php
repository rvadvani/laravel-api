@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    @foreach($articles as $article)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <small class="pull-right">{{ Carbon\Carbon::createFromTimeStamp(strtotime($article->created_at))->diffForHumans() }}</small>
                                {{ $article->title }} <small>({{ $article->topic->topic }})</small>
                            </div>

                            <div class="panel-body">
                                {{ $article->body }}
                                <hr>
                                <small>posted by: {{ $article->user->name }}</small>
                            </div>
                        </div>
                    @endforeach
                    {{ $articles->links() }}

            </div>
            <div class="col-md-4">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">By Topics</h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">By Users</h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">By Dates</h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse in">
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

