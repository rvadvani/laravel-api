@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('menu')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#post">Post Article</button>
                    Dashboard
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <br>

                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th>Action</th>
                            </tr>
                            <?php $id = 1; ?>
                            @foreach($articles as $article)
                                <tr>
                                    <td>{{ $id++ }}</td>
                                    <td>{{ $article->topic->topic }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ $article->body }}</td>
                                    <td>
                                        <a href="{{ url('/home/'.$article->id) }}" class="btn btn-sm btn-primary">edit</a>
                                        <button class="btn btn-danger btn-sm">delete</button>
                                    </td>
                                </tr>
                            @endforeach

                        </table>

                        {{ $articles->links() }}
                </div>
            </div>

            <!-- The Modal -->
            <div class="modal fade" id="post">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Post Article</h4>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <form action="{{ route('create.article') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <select name="topic_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($topics as $topic)
                                            <option value="{{ $topic->id }}">{{ $topic->topic }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Body</label>
                                    <input type="text" name="body" class="form-control">
                                </div>
                                <button class="btn btn-primary">Submit</button>
                            </form>.
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
