@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('menu')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Dashboard
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <h4>{{ $article->title }} <small>({{ $article->topic->topic }})</small></h4>
                        <p>{{ $article->body }}</p>

                        <small>posted on: {{ Carbon\Carbon::createFromTimeStamp(strtotime($article->created_at))->diffForHumans() }} - posted by: {{ $article->user->name }}</small>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
