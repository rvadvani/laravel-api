<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Article;

Route::get('/', function () {
    $articles = Article::orderBy('id', 'DESC')->paginate(5);
    return view('welcome', compact('articles'));
});



Auth::routes();
Route::post('/home', 'HomeController@create')->name('create.article');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{id}', 'HomeController@view');
Route::put('/home/{id}', 'HomeController@update')->name('update.article');
