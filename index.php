<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body
        {
            background-color: #fff;
            padding: 10px;
            font-family: Candara;
            color: #ff6d9d;
            font-size: 24px;
            text-shadow: 3px 3px 3px #d366ff;
            display: block;
        }
        header
        {
            padding: 20px 40px;
            border-bottom: 2px solid #ddd;
        }
        a
        {
            padding: 10px;
            text-decoration: none;
            font-family: Candara;
            color: #ff6d9d;
            font-size: 24px;
            text-shadow: 3px 3px 3px #d366ff;
            display: block;
        }
        aside
        {
            width: 12%;
            float: left;
        }
        article
        {
            width: 84%;
            float: left;
        }
    </style>
</head>
<body>

<header>
    xampp
</header>
<aside>
    <a href="laravel/">Laravel</a>
    <a href="wordpress/">Word Press</a>
    <a href="codeigniter/">Codeigniter</a>
</aside>
<article>
    Content
</article>
</body>
</html>